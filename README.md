# Image Cutter

Be sure to get the dependency:

```
pip install image_slicer
```

Run this script in the folder with your files that you want the left side for.

Developed for [07th Expansion Wiki](https://07th-expansion.fandom.com/wiki/07th_Expansion_Wiki)