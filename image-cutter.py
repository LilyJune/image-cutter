import os
import image_slicer

def iterate(directory):
    found = 0
    for entry in os.scandir(directory):
        if entry.path.endswith(".png") and entry.is_file():
            cutAndSave(entry.path)
            print('Found: ' + entry.path)
            found+=1

    if found == 0:
        print('no *.png images found')
    elif found >= 1:
        if not os.path.exists('./FinishedLeftHalves'):
            os.makedirs('FinishedLeftHalves')
        for entry in os.scandir(directory):
            if entry.path.endswith("_02.png") and entry.is_file():
                os.remove(entry.path)
            if entry.path.endswith("_01_01.png") and entry.is_file():
                dir = directory + '\\FinishedLeftHalves'
                path_length = len(directory)
                name = entry.path[path_length:-10]
                name = name + '.png'
                path = dir + name
                if not os.path.exists(path):
                    os.rename(entry.path, path)
                else:
                    print('File already exists skipping: ' + path)
        print('Your images can be found in the folder ' + dir)

def cutAndSave(path):
    tiles = (image_slicer.slice(path, 2))

if __name__ == "__main__":
    print(os.getcwd())
    iterate(os.getcwd())
    
